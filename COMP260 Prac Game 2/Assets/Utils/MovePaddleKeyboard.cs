﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeyboard : MonoBehaviour
{
    private Rigidbody rigidbody;
    public float speed = 20f;
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }
    

   
    public float maxSpeed = 5.0f;
    // Update is called once per frame
    void FixedUpdate()
    {
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");

        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;
		rigidbody.velocity = velocity;

    }
}
